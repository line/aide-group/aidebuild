#!/bin/bash

# Enters the gitlab instance URL and project ID

echo -n "Enter the GitLab instance URL [https://gitlab.inria.fr]: " ; read gitlab_URL
if [ -z "$gitlab_URL" ] ; then gitlab_URL="https://gitlab.inria.fr" ; fi
 
echo -n "Enter the GitLab project ID: " ; read project_ID

# Retrieves the project meta-data via the gitlab API

wget -q $gitlab_URL/api/v4/projects/$project_ID -O tmp-$$.txt

if [ "`ls -s tmp-$$.txt`" = "0 tmp-$$.txt" ]
then 
  echo "Error: bad GitLab instance URL = '$gitlab_URL' / Project ID = '$project_ID', is this a public project ?" 
  exit -1
fi

cat > tmp-$$.js <<EOF
// Parses the project meta data and extract what is needed here
const fs = require("fs");
let data = JSON.parse(""+fs.readFileSync("tmp-$$.txt"));
fs.writeFileSync("tmp-$$.sh",
 "name=\"" + data["name"] + "\"\n" +
 "description=\"" + data["description"] + "\"\n" +
 "git_url=\"" + data["ssh_url_to_repo"] + "\"\n" +
 "web_url=\"" + data["web_url"] + "\"\n" +
 "doc_url=\"" + data["web_url"].replace(new RegExp("([^/]*//)([^\\.]*)(\\.[^/]*/)(\\.[^/]*)/(.*)"),
  "\$1\$4.\$2\$3\$5").replace(".gitlab.", ".gitlabpages.")  + "\"\n");
EOF
node tmp-$$.js
sync
sync
. ./tmp-$$.sh
rm tmp-$$.*

# Creates the working directory

if [ -d "$name" ]
then 
  echo "The directory of name '$name' already exists"
elif [ -f "$name" ]
then
  echo "Error a file of name '$name' already exists, must stop"
  exit
else
  echo "Creating a working directory of name '$name'"
  mkdir -p "$name"
fi

cd "$name"

# Installs a package.json template file, if not yet done

if [ -f ./package.json ]
then 
  echo "The file $name/package.json already exists"
else
  echo "A $name/package.json is created, please edit and customize it"
  cat > ./package.json << EOF
{
  "name": "$name",
  "description": "$description",
  "homepage": "$doc_url",
  "version": "0.1.0",
  "license": "CECILL-C",
  "author": "\$Firstname Lastname <\$email>(\$url)",
  "repository": {
    "type": "git",
    "url": "$web_url",
    "project_ID": $project_ID
  },
  "usage" : [
    "\$usage-1st-line",
    "\$usage-2nd-line ."
  ],
  "scripts": {
    "install": "make -s --no-print-directory -C src install",
    "build":   "make -s --no-print-directory -C src build",
    "test":    "make -s --no-print-directory -C src test",
    "clean":   "make -s --no-print-directory -C src clean"
  },
  "dependencies": {
  },
  "devDependencies": {
    "aidebuild": "git+https://gitlab.inria.fr/line/aide-group/aidebuild.git"
  },
  "softwareheritage" : false,
  "npm_publish" : false,
  "build_cpp": true,
  "swig": false,
  "gypfile": false
}
EOF
fi

npm install > /dev/null 2>&1

# Installs a makefile if not yet done

if [ -f ./src/makefile ]
then 
  echo "The file $name/src/makefile already exists"
else
  echo "A $name/src/makefile is created"
  mkdir -p src
  cat > ./src/makefile <<EOF

### Defines C/C++ compilation options
CXX      = g++                               # 'CXX'          : defines the C/C++ compiler
CCFLAGS  =                                   # 'defines'      : compilation options
INCDIRS  =                                   # 'include_dirs' : source header directories
LIBDIRS  =                                   # 'library_dirs' : precompiled library directories
LIBS     =                                   # 'libraries'    : libraries to link with the code
AIDEDEPS = aidesys                           # 'AIDEDEPS'     : other aidebuild packages dependencies
# => These are illustrative examples, just to help, please adapt to your project.

### Includes aidebuild standard usage,build,test,clean,sync rules
include ./.makefile.inc
# => do not modify, unless you know why

### Complement BUILD targets
#BUILD = more_build
#
#more_build: 
#	echo "Here more build actions"
#endef

### Documents new usage of this makefile, if any
### -> decommenting the following lines to append to the USAGE doc
#define USAGE +=
# more: another make usage.
#
#endef

EOF
fi

if [ \! -f ./src/.makefile.inc ]
then
  echo "A $name/src/.makefile.inc is created"
  wget -q https://gitlab.inria.fr/line/aide-group/aidebuild/-/raw/master/src/makefile -O ./src/.makefile.inc
fi

# Defines the .gitignore file avoiding to commit npm installed files

if [ \! -f ./.gitignore ]
then 
  echo "A $name/.gitignore is created"
  cat > ./.gitignore <<EOF
*~
/build/
/node_modules/
/package-lock.json
EOF
fi

# Defines the .gitlab-ci.yml file avoiding to commit npm installed files

if [ \! -f ./.gitlab-ci.yml ]
then 
  echo "A ./.gitlab-ci.yml is created"
  cat > ./.gitlab-ci.yml <<EOF
pages:
  stage: deploy
  tags:
    - prod
  script:
    - echo ok
  only:
    - master
  artifacts:
    paths:
    - public
EOF
fi

if [ \! -f ./public/index.html ]
then
  echo "A ./public/index.html is created"
  mkdir -p public
  cat > ./public/index.html <<EOF
<script>location.replace('$web_url')</script>
EOF
fi

# Puts the file in a git directory of not yet done

if [ -d ./.git ]
then 
  echo "The directory $name/.git already exists"
else
  echo "Pushing files in the git directory"
  git init
  git remote add origin $git_url
  git add . 
  git add .gitignore .gitlab-ci.yml package.json src
  git commit -q -m "Initial commit"
  git push -u origin master
fi
