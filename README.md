# aidebuild

Builds multi-language compilation packages and related documentation.

@aideAPI

## Description

This packages provides utilities to develop and publish modular tools for web based applications.

- It is based on the [npmjs](https://www.npmjs.com) package system 

### Technical choices

The code is expected to be a set of [C/C++](http://www.cplusplus.com/reference) and [JavaScript](https://www.w3schools.com/jsref) source files, and a [Python](https://www.python.org) wrapper is available.

As a web service, it implements a local node.js server, and wrap C/C++ code using the [node-addon-api](https://github.com/nodejs/node-addon-examples), as defined the [aideweb](https://line.gitlabpages.inria.fr/aide-group/aideweb/) package.

The documentation uses [markdown syntax](https://www.markdownguide.org/basic-syntax) and [JsDoc tags](https://jsdoc.app/), with the [jsdoc](https://www.npmjs.com/package/jsdoc) mechanism and the [docdash](https://www.npmjs.com/package/docdash) theme. Documentation generation includes source files normalization (i.e., beautifying or uncrustification), multi-language documentation generation and production of images or PDF from LaTeX formula or latex or libreoffice drawing files.

### Coding rules

A few simple coding rules allows multi-language development to be smooth and easy. 

#### Notion of factory

When object oriented programming is not necessary, static methods are encapsulated in "factories", i.e., group of functions, accessible via the factory name, e.g., `aidesys::regexMatch(string, regex);` in C++ or `slugstring.fill(dictionary);` in JavaScript, thus without class object instance. 

- Naming conventions: 
  - Factory names are all in lower-case.
  - Static method names are verbs, in lower-case. Compounded names use the [camelCase](https://en.wikipedia.org/wiki/Camel_case) convention.
  - Static methods parameters names are nouns. Compounded parameters names hyphenation use the underscore `_` hyphen, including JSON tuples data names.

- Parameters type:
   - Boolean `bool`, integer `int` (or `uint` if unsigned), `double` (for floating point values) and `string` are the used scalar values (i.e. literals).
   - Otherwise, parameters are objects, defined via classes.

#### Notion of Classes

- For multi-language call, classes better use:
  - Parameter-less constructor (which is fine also for internal language use).
  - Setters: i.e., `set(value)` method to set the whole object, and `set(name, value)` to set an object parameter.
  - Getters: i.e., `get(name)` or `getName()` method to get an object parameter or `isName()` to get a boolean value parameter.
    - The `at(name)` method does not create a default value while the `get(name)` may create an entry for this parameter (in coherence with a subscript `[]` operator.
    - The object `size()` or `count()` methods are not prefixed by get in coherence with the main usage.
  - Casters: e.g., `asString()` to get the whole representation as a string (with similar convention for other convention).

- Naming conventions: 
  - Class names are nouns, instance names are verbs. Compounded names use the [camelCase](https://en.wikipedia.org/wiki/Camel_case) convention, methods parameters are nouns, using underscore `_` hyphen.
  - The `toString()` method is prohibited, because of Python wrapping.

<a name='what'></a>

## Package repository

- Package files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/aidebuild'>https://gitlab.inria.fr/line/aide-group/aidebuild</a>
- Package documentation: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild'>https://line.gitlabpages.inria.fr/aide-group/aidebuild</a>
- Source files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/aidebuild/-/tree/master/src'>https://gitlab.inria.fr/line/aide-group/aidebuild/-/tree/master/src</a>
- Saved on <a target='_blank' href='https://archive.softwareheritage.org/browse/origin/directory/?origin_url=https://gitlab.inria.fr/line/aide-group/aidebuild'>softwareherirage.org</a>
- Version `1.1.2`
- License `CECILL-C`

## Installation

### User simple installation

- `npm install git+https://gitlab.inria.fr/line/aide-group/aidebuild.git`

### Co-developper installation

- See the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html#.install_as_developer'>related documentation</a>

Please refer to the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html'>installation guide</a> for installation.

<a name='how'></a>

## Usage

### npm script usage
```
npm install --quiet : installs all package dependencies and sources.
npm run build: builds the different compiled, documentation and test files.
npm test     : runs functional and non-regression tests.
npm run clean: cleans installation files.
```

<a name='dep'></a>

## Dependencies

- <tt>cligui2: <a target='_blank' href='https://github.com/ThreeLetters/CLI-GUI2#readme'>ClI-GUI but better. Beautiful graphical interface on command line </a></tt>
- <tt>docdash: <a target='_blank' href='https://github.com/clenemt/docdash.git'>A clean, responsive documentation template theme for JSDoc 3 inspired by lodash and minami</a></tt>
- <tt>js-beautify: <a target='_blank' href='https://beautifier.io/'>beautifier.io for node</a></tt>
- <tt>jsdoc: <a target='_blank' href='https://github.com/jsdoc/jsdoc'>An API documentation generator for JavaScript.</a></tt>
- <tt>uglify-js: <a target='_blank' href='https://www.npmjs.com/package/uglify-js'>JavaScript parser, mangler/compressor and beautifier toolkit</a></tt>

<a name='who'></a>

## Author

- Thierry Viéville <thierry.vieville@inria.fr>

