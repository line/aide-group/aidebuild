#!/usr/bin/env node

/**
 * @function menu
 * @static
 * @description Generates a simple graphical user interface menu for the commands.
 * ```
 *     Usage: .../aidebuild/src/menu.js  [--term] [menu.json] &
 *      --term: runs in current terminal instead of opening a pop-up window.
 * ```
 * - It requires a `menu.json` file defining the shell commands to be run, e.g. :
 * ```
 * {
 *   " rebuild": "make rebuild what=ongoing",
 *   " clean":   "make clean",
 *   "console":  "mate-terminal",
 *   "suspend":  "sudo systemctl suspend"
 * }
 * ```
 * - Usage notes:
 *   - By convention, in the `menu.json` file:
 *      - command names starting with a space, e.g. `" rebuild"` or `" clean"` are run in a terminal,
 *      - others are run in background.
 *   - By default, `./menu.json`, else `.../aidebuild/src/menu.json`, is considered.
 *   - The menu title is built from:
 *      - the current directory base-name (not considering `.../src/` or `.../main/` suffix),
 *      - followed by the `menu.json` base-name (without extension, and if not equal to the word `menu`).
 *
 * - Implementation notes:
 *   - It requires a manual call of `npm install cligui2` on install.
 *   - The menu is generated using a `xterm` terminal graphic functionalities, yielding a menu of the form:
 * ![menu](./menu.png)
 *   - It does not require any specific GUI and it is usable on any platform.
 *   - It uses the [cligui2](https://www.npmjs.com/package/cligui2) clever middle-ware.
 */

const fs = require("fs");
const path = require("path");
const process = require("process");
const child_process = require("child_process");
const Cligui = require('cligui2');

// Error function
function exitOnError(cond, message) {
  if (cond) {
    console.log(process.argv[1] + " \t error: " + message);
    process.stdin.on("data", function() {});
    process.exit(-1);
  }
}

// Checks the command arguments
exitOnError(!(process.argv.length < 4 || (process.argv.length == 4 && process.argv[2] == "--term")), "bad usage, usage: .../menu.js [--term] [.../menu.json]");
let in_terminal = (process.argv.length == 3 || process.argv.length == 4) && process.argv[2] == "--term";

// Manages the default menu.json file
let menu_file = (process.argv.length == 2 || process.argv.length == 3 && process.argv[2] == "--term") ?
  (fs.existsSync("menu.json") ? "menu.json" : path.dirname(process.argv[1]) + "/menu.json") :
  process.argv[process.argv.length - 1];
exitOnError(!fs.existsSync(menu_file), "file '" + menu_file + "' not found.");

// Constructs the title
let t0 = path.basename(process.cwd()),
  t1 = menu_file.replace(new RegExp("\.json$"), ""),
  title = (t0 == "src" || t0 == "main" ? path.basename(path.dirname(process.cwd())) : t0) + (t1 == "menu" ? "" : "::" + t1);

// Loads the menu items
let menu_items = {};
try {
  menu_items = JSON.parse("" + fs.readFileSync(menu_file));
} catch (error) {
  exitOnError(true, "Parse error, reading the menu commands in '" + menu_file + "' ... '" + error + "'");
}
exitOnError(Object.keys(menu_items).length == 0, "no command in '" + menu_file + "'");

// Manages the popup window
if (!in_terminal) {

  // Calculates the window size
  let width = title.length,
    height = 5 + Object.keys(menu_items).length;
  for (let item of Object.keys(menu_items)) {
    if (width < item.length)
      width = item.length;
  }
  width += 4;

  let p = child_process.spawn("xterm", ["-title", "menu", "-geometry", "" + width + "x" + height, "-fa", "Monospace", "-fs", "12", "-e", "sh", "-c", process.argv[1] + " --term " + menu_file /* + " ; read cont"*/ ], {
    cwd: process.cwd(),
    env: process.env,
    detached: true
  });
  p.stdout.on('data', (data) => {
    console.log(`xterm stdout: ${data}`);
  });
  p.stderr.on('data', (data) => {
    exitOnError(true, `xterm stderr: ${data}`);
  });
  p.on('close', (status) => {
    exitOnError(status != 0, `xterm exit status: ${status}`);
  });
  p.unref();

} else {

  // Generates the menu scripts and starts
  let cligui = new Cligui();
  let menu_commands = {};
  for (let [item, command] of Object.entries(menu_items)) {
    menu_commands[item.trim()] = function() {
      child_process.exec(item.at(0) == ' ' ? "xterm -title menu -geometry 80x24 -fa 'Monospace' -fs 12 -e sh -c '" + command + " ; echo 'Done. Type Enter to finish' ; read cont' &" : command + " &", function(error, stdout, stderr) {
        if (stdout != "") {
          console.error(`${item} stdout: '${stdout}'`);
        }
        if (stderr != "") {
          console.error(`${item} stderr: '${stderr}'`);
        }
        if (error) {
          console.error(`${item} exec error: '${error}'`);
        }
        cligui.reset();
      });
    };
  }
  menu_commands["quit"] = function() {
    process.exit(0);
  }
  cligui.list(title, menu_commands);
}
