// Reduces AIDE specific macros in README (see also jsdoc2.js)

const process = require('process');
process.stdin.on("data", data => {
  let text = data.toString();

  text = text.replace(new RegExp("@aideAPI", "g"), "<div style='float:right;margin-left:20px'><a target=_blank' href='https://line.gitlabpages.inria.fr/aide-group/aide/index.html'><img alt='AIDE API docs' src='https://line.gitlabpages.inria.fr/aide-group/aide/img/icons8-api-aide.png'/></a></div>");
  text = text.replace(new RegExp("@slides\\s+([^\\s]+)", "g"), "<center><iframe style='width: 100%; height: calc(55vw);' src='$1'></iframe></center><a href='$1' target='_blank'>&nbsp;&nbsp;(open in new tab)</a>");

  process.stdout.write(text);
});
