#!/bin/sh

echo "The aide API system installation"
echo "  ref: https://line.gitlabpages.inria.fr/aide-group/aide/index.html"

#  Comment this line if you want to force install
if [  -z "`uname -v | grep Ubuntu`" ] ; then echo "." ; echo "Error: not on Ubuntu but '`uname -v`', exiting" ;  echo "." ;  exit -1 ; fi

echo "." ; echo "."
echo "  Running apt-get update, upgrade and install required packages" ; echo "."
# It is always good to have a system up to date :)
sudo apt-get update -q -y  ; sudo apt-get  upgrade -q -y
# Adapt some package names if not on standard recent ubuntu
sudo apt-get install -q -y make git nodejs npm wget curl libcurl4-openssh-dev python3-dev
# Comment this line if the linkchecker is never to be used
sudo apt-get install -q  -y linkchecker
# Comment this line if the swig python wrapper is never to be used
sudo apt-get install -q  -y swig
# Comment this line if opencv is never to be used
sudo apt-get install -q -y libopencv-core-dev libopencv-contrib-dev
# It is always good to clean an installation :)
sudo apt-get autoremove -q -y

echo "." ; echo "."
echo "  Install a recent version of node with a compatible version of npm and install required packages"  ; echo "."
# Installs the nodejs@v20.*.* repository for apt-get
curl -fsSL https://deb.nodesource.com/setup_20.x | sudo -E bash -
# Removes the unused doc (which is online) because it yields spurious installation
sudo apt remove -q -y nodejs-doc
# Now installs nodejs recent version
sudo apt install -q -y nodejs
# Imposes this  npm@10.1.0  because recent npm versions have git related bugs
sudo npm install -g npm@10.1.0
# It is always good to clean before installation :)
npm cache clean --force
# Now installs at global level all packages to avoid multiple local instances 
sudo npm install -g jsdoc docdash js-beautify uglify-js  bindings  express  express-fileupload node-addon-api

echo "." ; echo "."
echo " The aide API system installation is done" ; echo "."
