#!/bin/bash
parent="`pwd`" ; parent="`dirname $parent`" ; parent="`basename $parent`"
if [ "$parent" = "node_modules" ] 
then
# Production mode: Install the links to run the docdash template publish mechanism from aidebuild and the jsdoc2.js plugin
  mkdir -p ../jsdoc2
  ln -sf `pwd`/../docdash/static ../jsdoc2
  ln -sf `pwd`/../docdash/tmpl ../jsdoc2
  cp     `pwd`/src/jsdoc2/publish.js ../jsdoc2
  cp     `pwd`/src/jsdoc2/jsdoc2.js ../jsdoc/plugins
else
# Development mode: Install in the case we are in the aidebuild package itself
  mkdir -p node_modules/aidebuild
  cp -rf {public,package.json,README.md,src} node_modules/aidebuild 2>/dev/null
  cd node_modules/aidebuild ; bash src/install.sh
fi
