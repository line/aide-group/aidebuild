# Installing a gitlab page runner

## Install, once, the runner mechanism on a virtual machine

Run as root, on the virtual machine hosting the runners:

```
sudo su
wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
chmod +x /usr/local/bin/gitlab-runner
useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
/usr/local/bin/gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
/usr/local/bin/gitlab-runner start
/usr/local/bin/gitlab-runner status
```
obtaining a gitlab-runner running, as detailed in this [tutorial](https://gitlab.inria.fr/siteadmin/doc/-/wikis/faq#installation-example-on-a-gnulinux-slave-from-ciinriafr-ci-user-account).

To check if the gitlab-runner is running simply :
```
sudo /usr/local/bin/gitlab-runner status
```

## Install, for a group, a web page runner

### Get the runner registration token

- On the project group menu `Setting` => `General` => `CI/CD` open the `Runners` folder and go to `Set up a group Runner manually`
- Copy the $registration-token given by the interface

### Register the group gitlab-runner

On the virtual machine hosting the runners, register the runner :

```
sudo /usr/local/bin/gitlab-runner register
#> Enter the GitLab instance URL (for example, https://gitlab.com/):
# https://gitlab.inria.fr/
#> Enter the registration token:
# $registration-token 
#>Enter a description for the runner:
# $group-name page deploy
#> Enter tags for the runner (comma-separated):
# prod
#> Enter an executor:
# shell 
sudo /usr/local/bin/gitlab-runner list
```
_Remarks_: 

- The `$registration-token` corresponds to what is provided on the `Setting -> CI/CD -> Runners -> Set up a group Runner manually` section
- The `$group-name` is the name of the project on the gitlab platform to match runner and project

## Configure each project to have static pages active

- On Left Menu => `Settings` => `General` => `Visibility, project features, permissions` (clic `Expand`)
   - Enable `CI/CD` to Build, test, and deploy your changes.
   - Enable `Pages` … to host your static websites on GitLab.
   - Clic `Save Changes`.

- On Left Menu => `Settings` => `CI/CD` => `Runners` (clic `Expand`) 
   - Enable `Enable shared runners for this project`.
   - Clic `Save Changes`. (If no `Save Changes` button use the button of another `Settings` item).
   
- On Left Menu => `Deploy` => `Pages` 
   - Disable `Use unique domain option` in order to obtain a https://$group.gitlabpages.inria.fr/$group/$project standard URL

- Create a [`.gitlab-ci.yml`](https://gitlab.inria.fr/line/aide-group/jsdoc2/-/blob/master/.gitlab-ci.yml), if not yet done by [initialization](https://line.gitlabpages.inria.fr/aide-group/jsdoc2/init.html), commit and push.

### Verify the runner is started and get the related page link

- Wait up to 30 minutes for activation

- Go on the `Setting -> CI/CD -> Runners` menu and check that a there is a `Runners activated for this project, as [shown here](https://gitlab.inria.fr/sed-saclay/TutorielGitlabPages/-/raw/master/presentation/setup_check_runner_active.png)

- Push a version of the files with (i) the `.gitlab-ci.yml` file and (ii) a `public/index.html` minimal web page.

- Go on the `Setting -> CI/CD -> Pages` and check a page is defined and copy the related link




