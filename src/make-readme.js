#!/usr/bin/env node

/** 
 * @function make-readme
 * @static
 * @description Command line builder of the README.md file from the package.json file for aidebuild
 * ```
 *     Usage: ./src/make-readme.js
 * ```
 */

const fs = require("fs");

let input = JSON.parse(fs.readFileSync("package.json"));

// Generates details for package dependecies
function dep2readme(name, deps) {
  let input = fs.existsSync("./node_modules/" + name + "/package.json") ?
    JSON.parse(fs.readFileSync("./node_modules/" + name + "/package.json")) : {
      "name": name
    };
  if (!("homepage" in input)) {
    if ("repository" in input && input["repository"] instanceof Object && "url" in input["repository"]) {
      input["homepage"] = input["repository"]["url"];
    } else if (name in deps) {
      if (deps[name].match(new RegExp("^git+http.*\.git$"))) {
        input["homepage"] = deps[name].replace(new RegExp("^git+(.*)\.git$"), "$1");
      } else {
        // if (await urlExist("https://www.npmjs.com/package/"+name)) // with import urlExist from "url-exist"
        input["homepage"] = "https://www.npmjs.com/package/" + name;
      }
    }
  }
  if ("homepage" in input) {
    if ("description" in input) {
      return "- <tt>" + name + ": <a target='_blank' href='" + input["homepage"] + "'>" + input["description"] + "</a></tt>\n";
    } else {
      return "- <tt><a target='_blank' href='" + input["homepage"] + "'>" + name + "</a></tt>\n";
    }
  } else {
    if ("description" in input) {
      return "- <tt>" + name + ": " + input["description"] + "</tt>\n";
    } else {
      return "- <tt>" + name + "</tt>\n";
    }
  }
}

// Generates details for author declaration
function author2readme(author) {
  if (author instanceof Object) {
    return "- " +
      ("name" in author ? author["name"] : "") +
      ("email" in author ? "&nbsp; <big><a target='_blank' href='mailto:" + author["email"] + "'>&#128386;</a></big>" : "") +
      ("url" in author ? "&nbsp; <big><a target='_blank' href='" + author["url"] + "'>&#128463;</a></big>" : "") +
      "\n";
  } else {
    return "- " + author + "\n";
  }
}

// Generates the package output
let output =
  ("name" in input ?
    "# " + input["name"] + "\n" :
    "- No name ?!? … this is odd!\n") +
  ("description" in input ?
    "\n" + input["description"] + "\n" :
    "- No description … this is odd!\n") +
  (fs.existsSync("./src/introduction.md") ?
    "\n" + ("" + fs.readFileSync("./src/introduction.md")) /*.replace(new RegExp("^@[^\n]*$", "gm"), "\n")*/ :
    "") +
  "\n<a name='what'></a>\n" +
  "\n## Package repository\n\n" +
  ("repository" in input && input["repository"] instanceof Object && "url" in input["repository"] ?
    "- Package files: <a target='_blank' href='" + input["repository"]["url"] + "'>" + input["repository"]["url"] + "</a>\n" :
    "No package repository with an url … this is odd!\n") +
  ("homepage" in input ?
    "- Package documentation: <a target='_blank' href='" + input["homepage"] + "'>" + input["homepage"] + "</a>\n" :
    "- No Package documentation … this is odd!\n") +
  ("repository" in input && input["repository"] instanceof Object && "url" in input["repository"] ?
    "- Source files: <a target='_blank' href='" + input["repository"]["url"] + "/-/tree/master/src'>" + input["repository"]["url"] + "/-/tree/master/src</a>\n" :
    "") +
  ("softwareheritage" in input && input["softwareheritage"] == true && "repository" in input && input["repository"] instanceof Object && "url" in input["repository"] ?
    "- Saved on <a target='_blank' href='https://archive.softwareheritage.org/browse/origin/directory/?origin_url=" + input["repository"]["url"] + "'>softwareherirage.org</a>\n" :
    "") +
  ("version" in input ?
    "- Version `" + input["version"] + "`\n" :
    "- No version … this is odd!\n") +
  ("license" in input ?
    "- License `" + input["license"] + "`\n" :
    "- No license … this is odd!\n") +
  "\n## Installation\n" +
  ("repository" in input && input["repository"] instanceof Object && "url" in input["repository"] && "type" in input["repository"] && input["repository"]["type"] == "git" && "name" in input ?
    "\n### User simple installation\n\n- `npm install git+" + input["repository"]["url"].replace(new RegExp("\.git$"), "") + ".git`\n" +
    "\n### Co-developper installation\n\n- See the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html#.install_as_developer'>related documentation</a>\n" :
    "") +
  "\nPlease refer to the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html'>installation guide</a> for installation.\n" +
  "\n<a name='how'></a>\n" +
  "\n## Usage\n\n" +
  ("usage" in input ?
    "\n" + input["usage"] + "\n\n" :
    "") +
  "### npm script usage\n```\n" +
  "npm install --quiet : installs all package dependencies and sources.\n" +
  ("scripts" in input ?
    ("build" in input["scripts"] ?
      "npm run build: builds the different compiled, documentation and test files.\n" :
      "") +
    ("test" in input["scripts"] ?
      "npm test     : runs functional and non-regression tests.\n" :
      "") +
    ("clean" in input["scripts"] ?
      "npm run clean: cleans installation files.\n" :
      "") +
    "```\n" :
    "") +
  "\n<a name='dep'></a>\n" +
  ("dependencies" in input && input["dependencies"] instanceof Object ?
    "\n## Dependencies\n\n" + Object.keys(input["dependencies"]).map(name => dep2readme(name, input["dependencies"])).join("") :
    "\n## Dependencies\n\n- None\n") +
  ("devDependencies" in input && input["devDependencies"] instanceof Object ?
    "\n## devDependencies\n\n" + Object.keys(input["devDependencies"]).map(name => dep2readme(name, input["devDependencies"])).join("") :
    "") +
  "\n<a name='who'></a>\n" +
  ("author" in input ?
    "\n## Author\n\n" + author2readme(input["author"]) + "\n" :
    ("contributors" in input && Array.isArray(input["contributors"]) ?
      "\n## Authors\n\n" + input["contributors"].map(author2readme).join("") :
      "Neither author nor contributors … this is odd!\n")) +
  "";

fs.writeFileSync("README.md", output);
