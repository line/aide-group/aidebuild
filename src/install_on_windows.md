# Installing aide software on windows

As detailed here, there is a limited possibility to use aide-group software on windows.

## Installing cygwin

- Go to the https://www.cygwin.com/install.html site download the installation program.
   - Starts the `setup-x86_64.exe` program and allow it to execute on your machine.
   - Use all standard options for the installation.
     - Install from Internet. 
	 - Choose the standard root folder. 
	 - Install for everyone. 
	 - Choose the standard local download folder. 
	 - Choose the system proxy parameters. 
	 - Choose any mirror site. 
	 - Install all packages. 
	    - __Note__: Also look among not yet installed packages and add: `make`, `gcc-g++`, `git`, `wget`, [TO BE COMPLETED].
	 - Create an icon on the desktop and in the menu.
   - Open the Cygwin64 terminal present on the desktop to check that it works.

- Install https://nodejs.org/en/download/ outside cygwin at the windows level
   - Create a [.npmrc](https://docs.npmjs.com/cli/v6/configuring-npm/npmrc) with the following [npm options](https://docs.npmjs.com/cli/v6/using-npm/config):
   ```
   cache=~/.npm
   script-shell=bash.exe
   ```
- Interaction with other windows program
  - Note: antivirus software may consider cygwin program as potential intruders and usually conclude that they are safe, otherwise the quarantine should be contested.

## Installing the ssh key system on cygwin

- Create a secured computer ssh private/public key pair.
   - Create the key pair using, e.g, the `ssh-keygen -t rsa` command.
   - Choose the standard option to save the key in the `~/.ssh/id_rsa` file.
   - Copy the _public_ key pair on your gitlab account https://gitlab.inria.fr/-/profile/keys
_Note:_ Do not reuse an existing private/public key but create a new one (you can have several ssh keys active on gitlab).

Thanks to [this site](https://azaleasays.com/2014/02/05/remember-ssh-passphrase-in-cygwin) there is a [simple script](ssh_persitent.sh) to avoid entering the public key at each git operation, used using the `.` operator.
```
  . aidebuild/src/ssh_persistent.sh
```

## Installing a aide package to co-develop in C/C++

Let us consider, e.g., the [symboling](https://line.gitlabpages.inria.fr/aide-group/symboling/index.html) package, available on [gitlab](https://gitlab.inria.fr/line/aide-group/symboling)

- Open the Cywin64 terminal
- Use `mkdir -p aide` to create a dedicated folder and `cd aide` to go in it
- Clone the source bundle `git clone git@gitlab.inria.fr:line/aide-group/symboling.git`
   - This requires to have the ssh key installed as described previously
- Go in it `cd symboling` 
   - Run `npm install` to install
- Go in the `src` sub-directory
   - Run `make build_cpp` to compile the C/C++ files
   - Run `make test` to test the installation
- Reports any error, to thierry.vieville@inria.fr, this is still in development

## Limitations of using aide-group packages

- Actually, only the `aidebuild`, `aidesys`, `wjson`, `stepsolver` and `symboling` packages have been tested and validated.
  - Technically this means that only `build_cpp` has been validated.

- Actually `build_gyp`, `build_python` and `build_public` have not been tested, they sould work but likely with additional adaptation, yet to be done.
  - The workaround is to cooperate with someone under LinuX for the part of the work depending on these commands.
  
