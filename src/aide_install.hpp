/** @class install
 * @description Explains how to install packages built with aidebuild.
 */
/**
 * @function install_as_user
 * @memberof install
 * @static
 * @description Package installation to use the package.
 *
 * ## System installation
 *
 * - The required system installation, using the usual LinuX installer mechanism (either `apt-get` or `dnf` depending on the distribution) writes:
 * ```
 * # under fedora and equivalent
 *   sudo dnf install git nodejs npm wget curl libcurl-devel python3-devel
 * # under ubuntu, debian and equivalent (some package name adaptation may be required)
 *   sudo apt-get install git nodejs npm wget curl libcurl4-openssh-dev python3-dev
 * ```
 * The following global npm installation are recommended to avoid heavy local installation
 * ```
 * sudo npm install -g jsdoc docdash js-beautify uglify-js  bindings  express  express-fileupload node-addon-api
 * ```
 *
 * - A C++ compilation chain is also required and used for C/C++ compilation, but one is usually installed.
 * - A python installation is included for some packages and the Python version `3.10`, `3.11`,  `3.12`, or `3.7m` (on RasPI)   is actually in use.
 * - Complementary used tools are usual LinuX commands including [bash](https://en.wikipedia.org/wiki/Bash_(Unix_shell)).
 * - A web browser defined in the global `$BROWSER` environment variable is also used.
 *
 * ### Remarks:
 *  - Under _Ubuntu_ you may have to install dedicated versions of `nodejs` and `npm`, please refer to the [install_on_ubuntu.sh](./install_on_ubuntu.sh) documented script for details and easy use, after checking it is ok for you, you simply have to run the script.
 *  - This, presently, only works with a LinuX like distributions (including MacOS). A tutorial for a [windows partial installation](https://gitlab.inria.fr/line/aide-group/aidebuild/-/blob/master/src/install_on_windows.md) is available here.
 *  - If `npm` is already installed, you may have to run `sudo npm install -g npm@latest` to install the latest version or run `npm cache clean --force`  to clear spurious files.
 *  - If during `npm install` the `npm ERR! git dep preparation failed when trying to install package.json` there may several different causes
 *     -  `wget` has been forgotten to be installed,
 *     - a spurious version  of npm has been installed, use, e.g., `sudo npm install -g npm@10.1.0`, to overcome;
 *        - also do not hesitate to _copy the whole error message_ and [contact us](mailto:aide-help@inria.fr?subject=Installation%20problem%20with%aide%20tools).
 *
 * ### Complementary system installation
 *
 * - Depending on the package and usage the following installation are optional (without, the present mechanism will still work with reduced functionalities):
 *   - For [build_cpp](./make.html#.build_cpp), C/C++ debugging with [gdb](https://www.sourceware.org/gdb/) and C/C++ dynamic profiling with [valgrind](https://valgrind.org/) are used.
 *   - For [build_gyp](./make.html#.build_gyp), webservice construction uses [gyp](https://gyp.gsrc.io/index.md) mechanism.
 *   - For [build_python](./make.html#.build_python), python wrapping uses the [swig](http://www.swig.org).
 *   - For [build_public](./make.html#.build_public), documentation generation, the [linkchecker](https://wummel.github.io/linkchecker) tool, the LaTeX compilation chain, the [libreoffice](https://www.libreoffice.org/) software and [maple](https://www.maplesoft.com/) symbolic calculator, are optionally used.
 *
 * #### Installing opencv
 * - When using opencv there is a need to install, depending on the distribution:
 * ```
 * # under fedora and equivalent
 * sudo dnf     install opencv opencv-contrib opencv-devel
 * # under unbuntu and equivalent
 * sudo apt-get install libopencv-core-dev libopencv-contrib-dev
 * ```
 * see [install_rasppi](https://gitlab.inria.fr/line/aide-group/tabletop/-/blob/master/src/install_rasppi.md?ref_type=heads#installing-the-opencv-library) for more details on debian/RaspPi system
 *
 * - When implementing aide-group software on specific hardware such as [tabletop](https://line.gitlabpages.inria.fr/aide-group/tabletop/) on [Raspberry-Pi](https://www.raspberrypi.org/) and [esp32gpiocontrol](https://line.gitlabpages.inria.fr/aide-group/esp32gpiocontrol/) on [ESP32](https://en.wikipedia.org/wiki/ESP32), dedicated installation processes are documented within the package.
 *
 * - All other required tools are automatically installed and updated thanks to the npm mechanism, as defined in the `package.json` file, but:
 *    - The command: <br/> `sudo npm -g install docdash js-beautify jsdoc uglify-js` <br/> will install required package globally, and save local space and installation time.
 *
 * - Remark: Under some ubuntu instalation the some `npm warm EBADENGINE` and `errors in minimatch` occur withput imparing the installation.
 *
 * ## Installing a package
 *
 * - This is a npm package, installed using the npm command line tools. Here npm is used because it is a very simple mechanism with a large set of tools and an active and smart community, and it is particularly adapted to multi-language (C/C++ with Javacsript, and also Python) development, and very simple web-service design, including on small hardware such as the [Raspberry-Pi](https://www.raspberrypi.org/), and is easy to adapt to smaller connected objects.
 *
 * - Given a package in the `aide-group` of name, say, `wjson`, simply either/or:
 *   - Run in your node directory:
 * ```
 *  npm install git+https://gitlab.inria.fr/line/aide-group/wjson.git
 * ```
 *   - Add a line in the `package.json` file:
 * ```
 * {
 *  ../..
 *  "dependencies": {
 *    ../..
 *    "wjson": "git+https://gitlab.inria.fr/line/aide-group/wjson.git"
 *  }
 * }
 * ```
 */
/**
 * @function install_as_developer
 * @memberof install
 * @static
 * @description Package installation to co-develop the package.
 *
 * - This is a git source bundle of a npm package, installed using the git and npm command line tools.
 *
 * - You must start installing the required configuration as explained for [here](#.install_as_user).
 *
 * - To become a developer you need to:
 *   - Contact the aide support (e.g., [aide-help@inria.fr](mailto:aide-help@inria.fr?subject=Joining%20%the%20development%20aide%20team) to open a gitlab.inria external account (please explain your motivation).
 *   - Accept the invitation as co-developer of [https://gitlab.inria.fr/line/aide-group](https://gitlab.inria.fr/line/aide-group).".
 *   - Create a secured computer ssh private/public key pair.
 *      - Create the key pair using, e.g, the `ssh-keygen -t rsa` command.
 *      - Choose the standard option to save the key in the `~/.ssh/id_rsa` file.
 *      - Copy the _public_ key pair on your gitlab account [https://gitlab.inria.fr/-/profile/keys](https://gitlab.inria.fr/-/profile/keys)
 * _Note:_ Better do not reuse an existing private/public key but create a new one (you can have several ssh keys active on gitlab).
 *
 * - Given a package in the `aide-group` of name, say, `wjson`, simply either/or:
 *   - Choose a working directory.
 *   - In your working directory, clone the source bundle and change to the directory to install and test:
 * ```
 *  git clone git@gitlab.inria.fr:line/aide-group/wjson.git
 *  cd wjson
 *  npm install
 *  npm run build
 *  npm test
 * ```
 *
 *   - In order to recheck all elements rerun, in the package
 * ```
 *  git pull ; npm run clean ; npm install ; npm run build ; npm test
 * ```
 *
 *   - In order to co-develop, change to the src directory and use [make](make.html) directives.
 */
/**
 * @function install_a_new_package
 * @memberof install
 * @static
 * @description Initializes a package in order to be used with aidebuild.
 *
 * ## Initialization tutorial
 *
 * 0. You must be registered on the gitlab.inria.fr as [explained here](#.install_as_developer).
 * 1. Create a git [project](https://archives.docs.gitlab.com/16.3/ee/user/project/index.html) in the proper group.
 *   - Consider choosing public vibility
 *   - Enter a one line project desctription when creating the project or in the `Setting->Description` menu
 *   - Do not initialize repository with a `README.md`
 * 2. Note the `Project ID`
 *   - It is gettable from a little menu at the right of the project title
 *   - It is also readable on the `Setting => General` project menu page
 * 3. In the parent working directory, download and run `init.sh`
 *   - `wget -q https://gitlab.inria.fr/line/aide-group/aidebuild/-/raw/master/src/init.sh`
 *   - `sh ./init.sh`
 *     - This interactive script, asking for the `Project ID`, will generate a working directory with the proper initial files (See below).
 * 4. Edit and complete the `./package.json` meta-data file.
 *  - Please consider, beyond the [general documentation](https://docs.npmjs.com/cli/v8/configuring-npm/package-json):
 *    - [this simple example](https://gitlab.inria.fr/line/aide-group/aidebuild/-/blob/master/package.json) and
 *    - [this more sophisticated example](https://gitlab.inria.fr/line/aide-group/tabletop/-/blob/master/package.json).
 *  - Please refer to the [build_public](./make.html#.build_public) section for more information on specific meta-data options.
 * 5. Activate the [gitlab pages](https://gitlab.inria.fr/line/aide-group/aidebuild/-/blob/master/src/install_gitlab_page_runner.md#configure-each-projet-to-have-static-pages-active), to allow the package web-site to be activated, i.e.:
 * -On Left Menu => `Settings` => `General` => `Visibility, project features, permissions` (clic `Expand`)
 *    - Enable `CI/CD` to Build, test, and deploy your changes.
 *    - Enable `Pages` … to host your static websites on GitLab.
 *    - Clic `Save Changes`.
 * - On Left Menu => `Settings` => `CI/CD` => `Runners` (clic `Expand`)
 *   - Enable `Enable shared runners for this project`.
 *   - Clic `Save Changes`. (If no `Save Changes` button use the button of another `Settings` item).
 * - On Left Menu => `Deploy` => `Pages`
 *   - Disable `Use unique domain option` in order to obtain a https://$group.gitlabpages.inria.fr/$group/$project standard URL
 *
 * ## The package.json non-standard options
 *
 * The [package.json](https://docs.npmjs.com/cli/v9/configuring-npm/package-json) file is expected to contain:
 * - The "name", "description", "homepage" (of the documentation), "version", "license", "author" (or "contributors"), "repository" (of the source files), "scripts", "dependencies", "devDependecies" usual fields,
 * But since the specification is open, it may also contain:
 * - Multiple authors under the "contributors" field as exemplified [here](https://gitlab.inria.fr/line/aide-group/tabletop/-/blob/master/package.json), with "name", "email" and "url" items,
 * - In the "repository" field an additional "project_ID" item, corresponding to the project ID on the gitlab forge.
 * - A "usage" field which is a list a HTML lines, to complete the installation or usage documentation [more…](https://line.gitlabpages.inria.fr/aide-group/aidebuild/jsdoc2.html).
 * - A "softwareheritage" boolean field (default is false) to indicate that the software has been archived on https://archive.softwareheritage.org using [this link](https://archive.softwareheritage.org/save/list/).
 * - A "build_cpp" boolean field (default is true) to indicate that [C/C++ compilation](https://line.gitlabpages.inria.fr/aide-group/aidebuild/make.html#.build_cpp) has to be performed.
 * - A "swig" boolean field (default is false) to indicate the that the [python wrapper](https://line.gitlabpages.inria.fr/aide-group/aidebuild/make.html#.build_python) has to be built.
 * - A "gypfile" boolean field (default is false) to indicate that the [node-gyp wrapper](https://line.gitlabpages.inria.fr/aide-group/aidebuild/make.html#.build_gyp) has to be built.
 *
 * ## Initialization hints
 *
 * - Your default branch _must_ be `master`
 *   - to change (e.g. from `main`) to `master` consider the `Settings->Repository+Branch Defaults` menu
 *   - you also may have to unprotect the `main` branch in `Settings->Repository+Protected branches` menu
 *   - you better delete unused branches in `Code->Branches` menu
 *
 * ## Initialization details
 *
 * - This will create a directory with your project name and install the required files:
 *   - `./package.json` is the project package meta-data file, which is pre-formatted with all possible predefined information.
 *   - `./src/makefile` initial file, the required aidebuild include and variable templates, to partially clear if not used.
 *   - `./.gitignore` standard file, allowing to have clean git push and pull operations without local generated files.
 *   - `./.gitlab-ci.yml` standard file, allowing to generate a public web-site presenting and documenting the package, with a minimal `./public/index.html` page.
 *
 * - The present installation is specific to package on the `gitlab.inria.fr` forge but is easily adaptable to other platforms.
 * - You may also read the [init.sh](https://gitlab.inria.fr/line/aide-group/aidebuild/-/blob/master/src/init.sh) as a step by step tutorial and perform installation manually
 *
 */
