/** 
 * @class jsdoc2
 * @description Generates an improved version of documentation.
 *
 * ## Source code documentation
 *
 * - Source files are in the `./src` directory and documentation is generated in the `./public` directory.
 * - It uses [jsdoc](https://jsdoc.app) documentation mechanism for Javasctipt, C/C++ and shell source code.
 * - The [docdash](https://www.npmjs.com/package/docdash) template is used here.
 *
 * - With respect to standad jsdoc processing:
 *   - Classes with lower-case 1st letter are documented as "factory", i.e., set of static methods.
 *   - Inner classes are properly documented, in the navigation menu and highlighted in page.
 *   - The `@extends` tag must be inserted _after_ the `@description` because it is treated at the preprocessing level avoiding duplicating all methods in derived class, the [`@augments`](https://jsdoc.app/tags-augments.html) tag is still usable with standard usage.
 *   - Documentation footer is cleaned.
 *
 * - A few AIDE specific tags are introduced:
 *   - `@aideAPI`: inserts a right floating [icon](https://line.gitlabpages.inria.fr/aide-group/aide/img/icons8-api-aide.png) with a link to [AIDE API doc](https://line.gitlabpages.inria.fr/aide-group/aide/index.html).
 *   - `@slides $pdf-file`: inserts a iframe displaying the PDF slides generated by the local LateX mechanism in landscape mode.
 * 
 *
 * ### Other languages documentation
 *
 * Since multi-language (mainly C/C++ and bash/bat commands) is targeted
 *
 * - Files of suffixes .h, .hpp, .sh are scanned to extract the `/** ../.. ` comment sections, 
 * e.g., in the following `.sh` file, where the `#` prefix allows to comment the documentation lines.
 * 
 * ```
 * # /** This factory allows ... .
 * #  * @class factory_name
 * #  *&#47;
 * # /**
 * #  * @function function_name
 * #  * @memberof factory_name
 * #  * @static
 * #  * @description This function ... 
 * #  ../..
 * #  *&#47;
 * ```
 * 
 * ### Illustrative examples of documentation:
 *
 * - A Javascript class and inner method: [source](https://gitlab.inria.fr/line/aide-group/aideweb/-/blob/master/src/HttpQuery.js) and related [documentation](https://line.gitlabpages.inria.fr/aide-group/aideweb/HttpQuery.html)
 * - A Javascript factory: [source](https://gitlab.inria.fr/line/aide-group/wjson/-/blob/master/src/wJSON.js) and related [documentation](https://line.gitlabpages.inria.fr/aide-group/wjson/wJSON.html).
 * - A C/C++ Class and inner method: [source](https://gitlab.inria.fr/line/aide-group/wjson/-/blob/master/src/WJSONWriter.hpp) and [documentation](https://line.gitlabpages.inria.fr/aide-group/wjson/WJSONWriter.html).
 * - A C/C++ static procedure in a given namespace: [source](https://gitlab.inria.fr/line/aide-group/aidesys/-/blob/master/src/random.hpp) and related [documentation](https://line.gitlabpages.inria.fr/aide-group/aidesys/random.html).
 * - A shell script documentation: [source](https://gitlab.inria.fr/line/aide-group/creaspot/-/blob/master/src/wcreaspot.sh) and [documentation](https://line.gitlabpages.inria.fr/aide-group/creaspot/wcreaspot.html).
 * 
 *
 * ## README.md generation from the package.json
 *
 * - In the home page:
 *   - The `README.md` generated from the `package.json` is used for the home page.
 *
 * The [name](https://docs.npmjs.com/cli/v6/configuring-npm/package-json#name), 
 * one line [description](https://docs.npmjs.com/cli/v6/configuring-npm/package-json#description-1),
 * documentation [home page](https://docs.npmjs.com/cli/v6/configuring-npm/package-json#homepage),
 * [version](https://docs.npmjs.com/cli/v6/configuring-npm/package-json#version),
 * [license](https://docs.npmjs.com/cli/v6/configuring-npm/package-json#license),
 * [author](https://docs.npmjs.com/cli/v6/configuring-npm/package-json#people-fields-author-contributors) as a unique author or contributors,
 * git [repository](https://docs.npmjs.com/cli/v6/configuring-npm/package-json#repository) meta-data,
 * and an optional custom field `usage` (allowing to write more documentation)
 * are taken into account using the following format:
 *```
 * {
 *  "name": "$name",
 *  "description": "$one-line-description",
 *  "homepage": "$the-package-documentation-url",
 *  "version": "0.1.0",
 *  "license": "CECILL-C",
 *  "author": "$Firstname $Lastname <$email>($url), ../..",
 *  "repository": {
 *    "type": "git",
 *    "url": "$git-url"
 *  "usage" : [
 *    "$usage-1st-line",
 *    "$usage-2nd-line ../.."
 *  ],
 * ../..
 *```
 * while [dependencies](https://docs.npmjs.com/cli/v6/configuring-npm/package-json#dependencies),  [devDependencies](https://docs.npmjs.com/cli/v6/configuring-npm/package-json#devdependencies) and [test script](https://docs.npmjs.com/cli/v6/configuring-npm/package-json#scripts) are also taken into account.
 *
 * - The following standard `install`, `build`, `test`, `clean`, `sync` npm commands are documented.
 *
 * - It is assumed that the repository is a standard `git` repository, expliciting the source file location.
 *
 * - Several contributors acn also be defined, instead of the author, using the contsruct:
 *```
 *  "contributors": [
 *    { 
 *      "name": "$Firstname $Lastname", 
 *      "email": "$email", 
 *      "url": "$url" 
 *    },
 *   ../..
 *  ],
 *```
 *
 * ## Generation of complementary images, figures and LateX files.
 * 
 * - From the `./src` directory to the `./public` documentation directory:
 *   - Images in `.png` format are copied .
 *   - Figures in `.odg` libreoffice format are exported and copied.
 *   - LaTex documentation pages are generated using local `.tex` and `.bib` input files.
 *   - [Maple](https://en.wikipedia.org/wiki/Maple_(software)) figures and formula are generated runing the `.mpl` input file.
 *
 * ### Using LaTeX to generate complementary documents
 *
 * - Uses a [preamble](https://gitlab.inria.fr/line/aide-group/aide/-/blob/master/etc/www/tex/etc/preamble.tex) to the LaTeX source file in order to generate a landscape text presentation.
 * - Adds the ./src/*.bib references if \cite{} command.
 * - Provides a few additional LaTeX commands as detailled below.
 * - Manages *.png and *.odg image files the latex source file directory.
 * - Manages Maple *.mpl files to generates *.jpg figures or *.tex output, as detailled below.
 * - Shows the obtained pdf file if the -show options is present
 * - Notice: Target files are stored in the ./build/www/assets/tex directory, 
 *   - and an index.html page lists the generated files.
 *
 * - Warning: It is not possible to insert latex formula in source code documentation.
 *
 * #### The LaTeX preamble mechanism
 *
 * - LateX files of name `[a-z]*.tex` are compiled as landscape PDF documentation.
 *   - Files with names `_*.tex` are not compiled and considered as include files.
 *   - A LateX file can be used for both the documentation and an external draft, using the `\aidebuild` directive and conditional compilation:
 * ```
 * \ifdefined\aidebuild
 * % Part of text only appearing in aidebuild documentation
 * \else
 * % Part of text not appearing in aidebuild documentation
 * \fi
 * ```
 *
 *  This script uses a [preamble](https://gitlab.inria.fr/line/aide-group/aide/-/blob/master/etc/www/tex/etc/preamble.tex) which defines:
 *
 * - A landscape A4 PDF minimal display of text or plate, i.e., slide, to be displayed in the documentation bundle, using a structure of the form.
 * ```
 * \subsection*{Document title}
 * \subsubsection*{First page title}
 * % Here page content
 * \clearpage
 * \subsubsection*{Next page title}
 * % etc..
 * ```
 * without any preamble such as declaration of `\begin{document}` etc... and without `\end{document}` at the end. 
 * - Defines a few additional LaTeX commands as detailled below.
 *
 * This design choice allows you to both generate standard documentation pages and directly include the text, usually as appendix, in any other publication.
 *
 * #### The LaTeX additional commands
 *
 * - A minimal set of command to generate slide:
 *   - `\slide{#content}`: A 24 cm x 16.5 cm plate which contents is defined in `#content`.
 *   - `\stitle{#content}`: The slide title, if any.
 *   - `\scenter{#content}`, `\sright{#content}`: A centered, or right piece of text.
 *   - `\stwo{#left-width}{#right-width}{#left-content}{#right-content}`: Separate the slides in two collons so that `#left-width + #right-width = 1` with two contents.
 *
 * - A few miscellaneous commands:
 *   - `\deq`: Defines a `=` corresponding to a definition.
 *   - `\tab`: A 6 mm robust horizontal tabulation.
 *   - `\eqline{#math-content}`: A on-line minimal display of a mathematical equation.
 *   - `\hhref{#link}`: A link which text is the link itself.
 *
 * ### Using Maple to generate figures and formula
 *
 * In order to generate a figure the following Maple commands are available, as for example:
 * ```
 * plotsetup(jpeg, plotoutput="figure-name.jpg", plotoptions="width=600,height=600");
 * # Here a plot(../.. or plot3d(../.. command
 * plotsetup(x11);
 * ```
 * while a LaTeX command like this one, allows to insert the figure:
 * ```
 * \includegraphics[width=0.9\textwidth]{./figure-name.jpg}
 * ```
 *
 * _Using Maple to generate LaTeX formula_
 *
 * In order to generate a latex formula form LaTeX, a command of the form
 * ```
 * latex(Int(1/(x^2+1), x) = int(1/(x^2+1), x), "latex-file-name.tex");
 * ```
 * while a LaTeX command like this one, allows to insert the formula:
 * ```
 * \eqline{\input{latex-file-name.tex}}
 */
